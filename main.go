package main

import (
	"./service"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"os"
	"time"
)

const (
	DefaultConfigurationFilePath = "conf.json"
)

// entry point
func main() {
	// configuration file path
	configurationFilePath := DefaultConfigurationFilePath
	if len(os.Args) > 1 {
		configurationFilePath = os.Args[1]
	}

	// load configuration
	if configuration, err := service.BuildConfiguration(configurationFilePath); err == nil {
		// build service
		if srv, err := service.BuildService(common.GhostService[common.ServiceFrontEndWebAPI].DefaultPort,
			configuration); err == nil {
			for srv.IsRunning() {
				time.Sleep(time.Second)
			}
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		fmt.Println(err)
		os.Exit(1)
	}
}
