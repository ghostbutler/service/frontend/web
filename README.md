Web API server
==============

Generate statik resources
-------------------------

```bash
~/go/bin/statik -src=./res -dest=./service -f
```

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/service/frontend/desktop/tree/master
