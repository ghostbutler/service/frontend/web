package service

import (
	"./data"
	"./directory"
	"./html"
	_ "./statik"
	"bytes"
	"encoding/json"
	"errors"
	"github.com/rakyll/statik/fs"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

const (
	VersionMajor = 1
	VersionMinor = 0
)

type Service struct {
	// service
	service *common.Service

	// is running?
	isRunning bool

	// configuration
	configuration *Configuration

	// key manager
	keyManager *keystore.KeyManager

	// directory manager
	directoryManager *directory.Directory
	directoryData    data.Directory

	// statik filesystem
	statikFS http.FileSystem

	// template manager
	templateManager *html.Template
}

func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	// allocate service
	service := &Service{
		configuration: configuration,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceFrontEndWebAPI].Name),
		isRunning: true,
	}

	if fileSystem, err := fs.New(); err == nil {
		service.statikFS = fileSystem
	} else {
		return nil, err
	}

	if templateManager, err := html.BuildTemplate(service.statikFS); err == nil {
		service.templateManager = templateManager
	} else {
		return nil, err
	}

	service.directoryManager = directory.BuildDirectory(configuration.Directory.Hostname,
		&service.directoryData)

	// build service with previous specified data
	service.service = common.BuildService(listeningPort,
		common.ServiceFrontEndWebAPI,
		WebAPIService,
		VersionMajor,
		VersionMinor,
		nil,
		nil,
		configuration.SecurityManager.Hostname,
		service)

	// service is built
	return service, nil
}

// close service
func (service *Service) Close() {
	service.isRunning = false
}

// update
func (service *Service) updateThread() {
	for service.isRunning {

		// sleep
		time.Sleep(time.Second)
	}
}

// is running?
func (service *Service) IsRunning() bool {
	return service.isRunning
}

func (service *Service) SendRule(rule []byte) error {
	request, _ := http.NewRequest("POST",
		"https://"+
			service.configuration.Orchestrator.Hostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceOrchestrator].DefaultPort)+
			"/api/v1/orchestrator/rules",
		bytes.NewBuffer(rule))
	if response, err := common.InsecureHTTPClient.Do(request); err == nil {
		content, _ := ioutil.ReadAll(response.Body)
		_ = response.Body.Close()
		if response.StatusCode == http.StatusOK {
			return nil
		} else {
			e := make(map[string]string)
			_ = json.Unmarshal(content,
				&e)
			return errors.New(e["error"])
		}
	} else {
		return err
	}
}

func (service *Service) SendAction(rule []byte) error {
	request, _ := http.NewRequest("POST",
		"https://"+
			service.configuration.Orchestrator.Hostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceOrchestrator].DefaultPort)+
			"/api/v1/orchestrator/actions",
		bytes.NewBuffer(rule))
	if response, err := common.InsecureHTTPClient.Do(request); err == nil {
		content, _ := ioutil.ReadAll(response.Body)
		_ = response.Body.Close()
		if response.StatusCode == http.StatusOK {
			return nil
		} else {
			e := make(map[string]string)
			_ = json.Unmarshal(content,
				e)
			return errors.New(e["error"])
		}
	} else {
		return err
	}
}

func (service *Service) SendSchedule(schedule []byte) error {
	request, _ := http.NewRequest("POST",
		"https://"+
			service.configuration.Orchestrator.Hostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceOrchestrator].DefaultPort)+
			"/api/v1/orchestrator/schedules",
		bytes.NewBuffer(schedule))
	if response, err := common.InsecureHTTPClient.Do(request); err == nil {
		content, _ := ioutil.ReadAll(response.Body)
		_ = response.Body.Close()
		if response.StatusCode == http.StatusOK {
			return nil
		} else {
			e := make(map[string]string)
			_ = json.Unmarshal(content,
				e)
			return errors.New(e["error"])
		}
	} else {
		return err
	}
}
