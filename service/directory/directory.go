package directory

import (
	"../data"
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"time"
)

const (
	DelayBetweenDirectoryUpdate = time.Second * 1
)

type Directory struct {
	isRunning bool

	// hostname
	hostname string

	// data
	data *data.Directory
}

func BuildDirectory(hostname string,
	data *data.Directory) *Directory {
	directory := &Directory{
		data:      data,
		isRunning: true,
		hostname:  hostname,
	}
	go directory.update()
	return directory
}

func (directory *Directory) requestKnownType() ([]device.CapabilityType, error) {
	request, _ := http.NewRequest("GET",
		"https://"+
			directory.hostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceDirectory].DefaultPort)+
			"/api/v1/directory/types",
		nil)
	if response, err := common.InsecureHTTPClient.Do(request); err == nil {
		defer common.Close(response.Body)
		var capabilityList []string
		content, _ := ioutil.ReadAll(response.Body)
		if err := json.Unmarshal(content,
			&capabilityList); err == nil {
			var output []device.CapabilityType
			for _, capability := range capabilityList {
				output = append(output,
					device.FindCapabilityFromName(capability))
			}
			return output, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}

func (directory *Directory) requestDeviceForType(capabilityType device.CapabilityType) ([]string, error) {
	request, _ := http.NewRequest("GET",
		"https://"+
			directory.hostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceDirectory].DefaultPort)+
			"/api/v1/directory/list?type="+
			device.CapabilityTypeName[capabilityType],
		nil)
	if response, err := common.InsecureHTTPClient.Do(request); err == nil {
		defer common.Close(response.Body)
		var result []string
		content, _ := ioutil.ReadAll(response.Body)
		if err := json.Unmarshal(content,
			&result); err == nil {
			return result, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}

func (directory *Directory) update() {
	for directory.isRunning {
		if capabilityList, err := directory.requestKnownType(); err == nil {
			newDeviceList := make(map[device.CapabilityType][]string)
			for _, capability := range capabilityList {
				if deviceList, err := directory.requestDeviceForType(capability); err == nil {
					sort.Strings(deviceList)
					newDeviceList[capability] = deviceList
				}
			}

			directory.data.Lock()
			directory.data.CapabilityList = capabilityList
			directory.data.DeviceList = newDeviceList
			directory.data.Unlock()
		}
		time.Sleep(DelayBetweenDirectoryUpdate)
	}
}
