package data

import (
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"net/http"
)

type RuleCreation struct {
	Message string

	CapabilityList  []string
	DeviceList      map[string][]string
	FieldIdentifier map[string][]string

	ChosenCapability string
	RuleField        []orchestration.RuleFieldDescription
}

func BuildRuleCreation(request *http.Request,
	directoryData *Directory) *RuleCreation {
	// build basis
	ruleCreation := &RuleCreation{
		CapabilityList: make([]string,
			0,
			1),
		DeviceList: make(map[string][]string),
	}
	capabilityList := directoryData.CapabilityList
	deviceList := directoryData.DeviceList
	for _, capability := range capabilityList {
		ruleCreation.CapabilityList = append(ruleCreation.CapabilityList,
			device.CapabilityTypeName[capability])
	}
	for capability, deviceList := range deviceList {
		ruleCreation.DeviceList[device.CapabilityTypeName[capability]] = deviceList
	}

	// process request
	if err := request.ParseForm(); err == nil {
		ruleCreation.Message = common.ExtractFormValue(request,
			"message")

		ruleCreation.ChosenCapability = common.ExtractFormValue(request,
			"capability")
		if device.IsCapacityValid(ruleCreation.ChosenCapability) {
			ruleCreation.RuleField = orchestration.RuleFieldIdentifierDescription[device.FindCapabilityFromName(ruleCreation.ChosenCapability)]
		} else {
			ruleCreation.ChosenCapability = ""
		}
	}

	// done
	return ruleCreation
}
