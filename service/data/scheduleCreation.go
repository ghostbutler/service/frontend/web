package data

import (
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"io/ioutil"
	"net/http"
	"strconv"
)

type ScheduleCreation struct {
	Message string

	RuleList   []orchestration.Rule
	ActionList []orchestration.Action
}

// build schedule creation
func BuildScheduleCreation(orchestratorHostname string,
	keyManager *keystore.KeyManager) (*ScheduleCreation, error) {
	scheduleCreation := &ScheduleCreation{}

	if request, err := http.NewRequest("GET",
		"https://"+
			orchestratorHostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceOrchestrator].DefaultPort)+
			"/api/v1/orchestrator/rules/list",
		nil); err == nil {
		if key, err := keyManager.GetOneUseKey(); err == nil {
			request.Header.Add(keystore.OneTimeKeyShareHeaderName,
				key.PublicKey)
			if response, err := common.InsecureHTTPClient.Do(request); err == nil {
				content, _ := ioutil.ReadAll(response.Body)
				_ = response.Body.Close()
				if err := json.Unmarshal(content,
					&scheduleCreation.RuleList); err != nil {
					return nil, err
				}
			} else {
				return nil, err
			}
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}

	if request, err := http.NewRequest("GET",
		"https://"+
			orchestratorHostname+
			":"+
			strconv.Itoa(common.GhostService[common.ServiceOrchestrator].DefaultPort)+
			"/api/v1/orchestrator/actions/list",
		nil); err == nil {
		if key, err := keyManager.GetOneUseKey(); err == nil {
			request.Header.Add(keystore.OneTimeKeyShareHeaderName,
				key.PublicKey)
			if response, err := common.InsecureHTTPClient.Do(request); err == nil {
				content, _ := ioutil.ReadAll(response.Body)
				_ = response.Body.Close()
				if err := json.Unmarshal(content,
					&scheduleCreation.ActionList); err != nil {
					return nil, err
				}
			} else {
				return nil, err
			}
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}

	return scheduleCreation, nil
}
