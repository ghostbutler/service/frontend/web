package data

import (
	"gitlab.com/ghostbutler/tool/service/device"
	"sync"
)

type Directory struct {
	sync.Mutex

	// capability list
	CapabilityList []device.CapabilityType

	// device name list
	DeviceList map[device.CapabilityType][]string
}

// is device known?
func (directory *Directory) IsDeviceKnown(capability device.CapabilityType,
	name string) bool {
	directory.Lock()
	defer directory.Unlock()
	if deviceList, ok := directory.DeviceList[capability]; ok {
		for _, dvc := range deviceList {
			if dvc == name {
				return true
			}
		}
	}
	return false
}
