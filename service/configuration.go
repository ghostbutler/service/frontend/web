package service

import (
	"encoding/json"
	"io/ioutil"
)

// configuration
type Configuration struct {
	SecurityManager struct {
		Hostname string `json:"hostname"`

		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"securityManager"`

	Orchestrator struct {
		Hostname string `json:"hostname"`
	} `json:"orchestrator"`

	Directory struct {
		Hostname string `json:"hostname"`
	} `json:"directory"`
}

// build configuration
func BuildConfiguration(configurationFilePath string) (*Configuration, error) {
	// configuration
	configuration := &Configuration{}

	// load file
	if fileContent, err := ioutil.ReadFile(configurationFilePath); err == nil {
		// unmarshal
		if err := json.Unmarshal(fileContent,
			configuration); err == nil {
			return configuration, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}
