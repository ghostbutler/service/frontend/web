package service

import (
	"./data"
	"./html"
	"encoding/json"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/orchestration"
	"net/http"
	"strconv"
	"time"
)

const (
	// home page
	APIServiceV1WebApiHomePage = common.APIServiceType(iota + common.APIServiceBuiltInLast)

	// create a rule
	APIServiceV1WebApiCreateRuleForm
	APIServiceV1WebApiCreateRule

	// create an action
	APIServiceV1WebApiCreateActionForm
	APIServiceV1WebApiCreateAction

	// create a schedule
	APIServiceV1WebApiCreateScheduleForm
	APIServiceV1WebApiCreateSchedule

	// login endpoint
	// todo remove login endpoint from directory
	APIServiceV1WebApiLogin
)

// declare how you want your endpoints to be accessed to
// Path is the split version of URL.Path by '/' character. So to express /api/v1/first, you'll put here
// ["api", "v1", "first"]
// Method is the HTTP method to use to access this endpoint
// Callback is a function of type common.EndpointFunction which will be called when an HTTP request triggers an endpoint
var WebAPIService = map[common.APIServiceType]*common.APIEndpoint{
	APIServiceV1WebApiHomePage: {
		Path:                    []string{},
		Method:                  "GET",
		Description:             "Homepage",
		Callback:                WebAPIEndpointHomepage,
		IsMustProvideOneTimeKey: false,
	},

	APIServiceV1WebApiCreateRuleForm: {
		Path:                    []string{"create", "rule"},
		Method:                  "GET",
		Description:             "Create a rule",
		Callback:                WebAPIEndpointCreateRuleForm,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1WebApiCreateRule: {
		Path:                    []string{"create", "rule"},
		Method:                  "POST",
		Description:             "Create a rule",
		Callback:                WebAPIEndpointCreateRule,
		IsMustProvideOneTimeKey: false,
	},

	APIServiceV1WebApiCreateActionForm: {
		Path:                    []string{"create", "action"},
		Method:                  "GET",
		Description:             "Create an action",
		Callback:                WebAPIEndpointCreateActionForm,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1WebApiCreateAction: {
		Path:                    []string{"create", "action"},
		Method:                  "POST",
		Description:             "Create an action",
		Callback:                WebAPIEndpointCreateAction,
		IsMustProvideOneTimeKey: false,
	},

	APIServiceV1WebApiCreateScheduleForm: {
		Path:                    []string{"create", "schedule"},
		Method:                  "GET",
		Description:             "Create schedule",
		Callback:                WebAPIEndpointCreateScheduleForm,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1WebApiCreateSchedule: {
		Path:                    []string{"create", "schedule"},
		Method:                  "POST",
		Description:             "Create a schedule",
		Callback:                WebAPIEndpointCreateSchedule,
		IsMustProvideOneTimeKey: false,
	},

	APIServiceV1WebApiLogin: {
		Path:                    []string{"api", "v1", "login"},
		Method:                  "POST",
		Description:             "Login (username/password/OTP)",
		Callback:                WebAPIEndpointLogin,
		IsMustProvideOneTimeKey: false,
	},
}

func WebAPIEndpointHomepage(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	_ = srv.templateManager.RenderTemplate(rw,
		html.PageHomepage,
		nil)
	return http.StatusOK
}

func WebAPIEndpointCreateRuleForm(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	r := data.BuildRuleCreation(request,
		&srv.directoryData)
	if err := srv.templateManager.RenderTemplate(rw,
		html.PageCreateRule,
		r); err != nil {
		fmt.Println(err)
	}
	return http.StatusOK
}

func WebAPIEndpointCreateRule(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	var message string

	if err := request.ParseForm(); err == nil {
		var rule orchestration.Rule
		content := common.ExtractFormValue(request,
			"rule")
		if err := json.Unmarshal([]byte(content),
			&rule); err == nil {
			rule.UUID = "?"
			if err := rule.IsValid(); err == nil {
				if err := srv.SendRule(rule.MarshalJson()); err != nil {
					message = err.Error()
				}
			} else {
				message = err.Error()
			}
		} else {
			message = err.Error()
		}
	}

	// redirect
	http.Redirect(rw,
		request,
		"/create/rule?message="+
			message,
		http.StatusFound)
	return http.StatusFound
}

func WebAPIEndpointCreateActionForm(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	r := data.BuildActionCreation(request,
		&srv.directoryData)
	if err := srv.templateManager.RenderTemplate(rw,
		html.PageCreateAction,
		r); err != nil {
		fmt.Println(err)
	}
	return http.StatusOK
}

func WebAPIEndpointCreateAction(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	var message string

	if err := request.ParseForm(); err == nil {
		var action orchestration.Action
		content := common.ExtractFormValue(request,
			"action")
		if err := json.Unmarshal([]byte(content),
			&action); err == nil {
			action.UUID = "?"
			if err := action.IsValid(); err == nil {
				if err := srv.SendAction(action.MarshalJson()); err == nil {

				} else {
					message = err.Error()
				}
			} else {
				message = err.Error()
			}
		} else {
			message = err.Error()
		}
	}

	// redirect
	http.Redirect(rw,
		request,
		"/create/action?message="+
			message,
		http.StatusFound)
	return http.StatusFound
}

func WebAPIEndpointCreateScheduleForm(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	_ = request.ParseForm()
	if scheduleCreation, err := data.BuildScheduleCreation(srv.configuration.Orchestrator.Hostname,
		srv.keyManager); err == nil {
		scheduleCreation.Message = common.ExtractFormValue(request,
			"message")
		_ = srv.templateManager.RenderTemplate(rw,
			html.PageCreateSchedule,
			scheduleCreation)
		return http.StatusOK
	} else {
		rw.WriteHeader(http.StatusInternalServerError)
		_, _ = rw.Write([]byte("{\"error\":\"" +
			err.Error() +
			"\"}"))
		return http.StatusInternalServerError
	}
}

func WebAPIEndpointCreateSchedule(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	var message string
	if err := request.ParseForm(); err == nil {
		var delayNumber int
		if delayNumber, err = strconv.Atoi(common.ExtractFormValue(request,
			"ruleTriggerInvalidateDelayNumber")); err == nil {
			schedule := orchestration.Schedule{
				Basis: orchestration.Basis{
					Name: common.ExtractFormValue(request,
						"name"),
					Description: common.ExtractFormValue(request,
						"description"),
					UUID: "?",
				},
				ActionId: request.Form["actionList"],
				RuleId:   request.Form["ruleList"],
				RuleMode: orchestration.ScheduleRuleMode(common.ExtractFormValue(request,
					"ruleModeGroup")),
				TriggerReactivationMode: orchestration.ScheduleTriggerReactivationMode(common.ExtractFormValue(request,
					"ruleTriggerInvalidateModeGroup")),
				Time: orchestration.ScheduleTime{
					IsActive:    true,
					ValidDay:    []orchestration.WeekDay{"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"},
					Periodicity: delayNumber,
					PeriodicityUnit: orchestration.TimeUnit(common.ExtractFormValue(request,
						"ruleTriggerInvalidateDelayGroup")),
					CreationDate: time.Now(),
				},
			}
			if scheduleCreation, err := data.BuildScheduleCreation(srv.configuration.Orchestrator.Hostname,
				srv.keyManager); err == nil {
				if err := schedule.IsValid(scheduleCreation.RuleList,
					scheduleCreation.ActionList); err == nil {
					if err := srv.SendSchedule(schedule.MarshalJson()); err != nil {
						message = err.Error()
					}
				} else {
					message = "invalid schedule: " +
						err.Error()
				}
			} else {
				message = "can't contact the orchestrator"
			}
		}
	} else {
		message = "bad form"
		fmt.Println(err)
	}

	// redirect
	http.Redirect(rw,
		request,
		"/create/schedule?message="+
			message,
		http.StatusFound)
	return http.StatusFound
}

func WebAPIEndpointLogin(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	return http.StatusOK
}
