package html

import (
	"errors"
	"github.com/oxtoacart/bpool"
	"html/template"
	"io/ioutil"
	"net/http"
)

// main template
const MainTemplateContent = `{{define "main" }}{{ template "page" . }}{{ end }}`

type Info struct {
	Path string
	Name string
}

const (
	PageHomepage       = "homepage"
	PageCreateRule     = "createRule"
	PageCreateAction   = "createAction"
	PageCreateSchedule = "createSchedule"
)

var (
	LayoutFileList = []Info{
		{
			Path: "/html/layout/page.html",
			Name: "page",
		},
		{
			Path: "/html/layout/head.html",
			Name: "header",
		},
		{
			Path: "/html/layout/footer.html",
			Name: "footer",
		},
	}

	PageFileList = []Info{
		{
			Path: "/html/homepage.html",
			Name: PageHomepage,
		},
		{
			Path: "/html/createRule.html",
			Name: PageCreateRule,
		},
		{
			Path: "/html/createAction.html",
			Name: PageCreateAction,
		},
		{
			Path: "/html/createSchedule.html",
			Name: PageCreateSchedule,
		},
	}
)

type Template struct {
	templateList map[string]*template.Template
	bufferPool   *bpool.BufferPool
}

func BuildTemplate(statikFS http.FileSystem) (*Template, error) {
	output := &Template{
		bufferPool:   bpool.NewBufferPool(64),
		templateList: make(map[string]*template.Template),
	}
	mainTemplate := template.New("main")
	if mainTemplate, err := mainTemplate.Parse(MainTemplateContent); err == nil {
		for _, templateFileInfo := range PageFileList {
			fileList := append(LayoutFileList,
				templateFileInfo)
			if output.templateList[templateFileInfo.Name], err = mainTemplate.Clone(); err != nil {
				return nil, err
			}
			for _, fileName := range fileList {
				if file, err := statikFS.Open(fileName.Path); err == nil {
					fileContent, _ := ioutil.ReadAll(file)
					if output.templateList[templateFileInfo.Name], err = output.templateList[templateFileInfo.Name].Parse(
						string(fileContent)); err != nil {
						return nil, err
					}
				} else {
					return nil, err
				}
			}
		}
		return output, nil
	} else {
		return nil, err
	}
}

func (tmpl *Template) RenderTemplate(responseWriter http.ResponseWriter,
	name string,
	data interface{}) error {
	if templateInstance, ok := tmpl.templateList[name]; ok {
		buf := tmpl.bufferPool.Get()
		defer tmpl.bufferPool.Put(buf)
		if err := templateInstance.Execute(buf, data); err != nil {
			return err
		}
		responseWriter.Header().Set("Content-Type", "text/html; charset=utf-8")
		_, _ = buf.WriteTo(responseWriter)
		return nil
	} else {
		return errors.New("template doesn't exist")
	}
}
